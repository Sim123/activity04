public class Application {
	public static void main(String[] args) {
		
		Dog labrador  = new Dog("Labrador", 12, 28, 6);
		System.out.println("The dog's breed is " + labrador.getBreed());
		System.out.println("It can run at " + labrador.getAverageSpeed() + " mph.");
		System.out.println("A " + labrador.getBreed() + " will live for an average of " + labrador.getLifeSpan() + " years");
		
		Dog chihuahua  = new Dog("Chihuahua", 20, 15, 3);
		chihuahua.setBreed("Chihuahua");
		chihuahua.lifeSpan = 20;
		chihuahua.averageSpeed = 15;
		chihuahua.setAge(3);
		System.out.println("The dog's breed is " + chihuahua.getBreed());
		System.out.println("It can run at " + chihuahua.getAverageSpeed() + " mph.");
		System.out.println("A " + chihuahua.getBreed() + " will live for an average of " + chihuahua.getLifeSpan() + " years");
		System.out.println("The age is " + chihuahua.growUp());
		chihuahua.setAge(chihuahua.growUp());
		System.out.println("The age is " + chihuahua.growUp());
		chihuahua.setAge(chihuahua.growUp());
		System.out.println("The age is " + chihuahua.growUp());
	}
}
