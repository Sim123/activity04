public class Dog {
	private String breed;
	private int lifeSpan;
	private int averageSpeed;
	private int age;
	
	public String getBreed() {
		return this.breed;
		}
	
	public int getLifeSpan() {
		return this.lifeSpan;
		}
	
	public int getAverageSpeed() {
		return this.averageSpeed;
		}
	
	public int getAge() {
		return this.age;
		}
	
	//we are here
		public void setBreed(String newBreed) {
			this.breed = newBreed;
			}
	
		public void setAge(int newAge) {
			this.age = newAge;
			}
	
	public Dog(String newBreed, int lifeSpan, int averageSpeed, int age){
		this.breed = "Dog";
		this.lifeSpan = 10;
		this.averageSpeed = 15;
		this.age = 5;
		}
	
	public int growUp(){
		return getAge() + 1;
	}
}
